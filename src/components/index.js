import Vue from 'vue';
import Menu from './Menu';
import Home from './Home';

export {
    Menu,
    Home
}
