import Vue from 'vue';
import Router from 'vue-router';
import { Menu, Home } from '@/components';
import CreatePicturePage from '@/components/CreatePicturePage'
import OrderPicturesPage from '@/components/OrderPicturesPage'
import UpdatePicturePage from '@/components/UpdatePicturePage'
import CategoriesPage from '@/components/CategoriesPage'
import { components, AmplifyEventBus } from 'aws-amplify-vue';
import Amplify, * as AmplifyModules from 'aws-amplify';
import { AmplifyPlugin } from 'aws-amplify-vue';
import AmplifyStore from '../store/store';


Vue.use(Router);
Vue.use(AmplifyPlugin, AmplifyModules);

let user;

getUser().then((user, error) => {
    if (user) {
        router.push({path: '/'})
    }
})


AmplifyEventBus.$on('authState', async (state) => {
    if (state === 'signedOut'){
        user = null;
        AmplifyStore.commit('setUser', null);
        router.push({path: '/auth'})
    } else if (state === 'signedIn') {
        user = await getUser();
        router.push({path: '/'})
    }
});

function getUser() {
    return Vue.prototype.$Amplify.Auth.currentAuthenticatedUser().then((data) => {
        if (data && data.signInUserSession) {
            AmplifyStore.commit('setUser', data);
            return data;
        }
    }).catch((e) => {
        AmplifyStore.commit('setUser', null);
        return null
    });
}

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
            meta: { requiresAuth: true}
        },
        {
            path: '/categories',
            name: 'CategoriesPage',
            component: CategoriesPage,
            meta: { requiresAuth: true}
        },
        {
            path: '/picture/update/:uuid',
            name: 'UpdatePicturePage',
            component: UpdatePicturePage,
            meta: { requiresAuth: true}
        },
        {
            path: '/categories/order-pictures',
            name: 'OrderPicturesPage',
            component: OrderPicturesPage,
            meta: { requiresAuth: true}
        },

        {
            path: '/picture/create',
            name: 'CreatePicturePage',
            component: CreatePicturePage,
            meta: { requiresAuth: true}
        },
        {
            path: '/auth',
            name: 'Authenticator',
            component: components.Authenticator
        }
    ]
});

router.beforeResolve(async (to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        user = await getUser();
        if (!user) {
            return next({
                path: '/auth',
                query: {
                    redirect: to.fullPath,
                }
            });
        }
        return next()
    }
    return next()
})

export default router
